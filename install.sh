cp xinitrc ~/.xinitrc
cp Xmodmap ~/.Xmodmap
cp -pr config/* ~/.config/

# cmake and libtool are required by vterm
# picom is a compositor, used for transparency
# feh is used for background
sudo xbps-install -S git xorg leftwm polybar lua fish-shell htop pfetch emacs-gtk3 elogind kitty dmenu pamixer xbacklight slock cmake make libtool picom feh redshift pipewire NetworkManager libspa-bluetooth bluez
chsh -s /usr/bin/fish

sudo mkdir -p /etc/pipewire/pipewire.conf.d
sudo ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
sudo ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/

sudo ln -s /etc/sv/elogind /var/service
sudo ln -s /etc/sv/dbus /var/service
sudo ln -s /etc/sv/alsa /var/service
sudo ln -s /etc/sv/NetworkManager /var/service
sudo ln -s /etc/sv/bluetoothd /var/service


