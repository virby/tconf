#!/bin/env lua

local settings = {
   displayName = false,
}

local function split(string, sep)
   local t = {}

   for str in string.gmatch(string, "([^"..sep.."]+)") do
      table.insert(t, str)
   end
   return t
end

local function capture(cmd)
   local f = assert(io.popen(cmd, 'r'))
   local s = assert(f:read('*a'))
   f:close()
   s = string.gsub(s, '^%s+', '')
   s = string.gsub(s, '%s+$', '')
   return s
end

local passwd = capture("getent passwd")
local uid = capture("id -u")
for line in passwd:gmatch("([^\n]*)\n?") do
   local info = split(line, ":")
   if info[3] == uid then
      local name = info[1]
      local gecos = info[5]
      local display = split(gecos, ",")[1]
      if settings.displayName then
         print(display.." ("..name..")")
      else
         print(name)
      end
      break
   end
end
