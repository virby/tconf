;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #ff222222
foreground = #dfdfdf
background-alt = #99444444
foreground-alt = #FFFFFF
primary = #ffb52a
secondary = #e60053
alert = #bd2c40
warn = #ffb52a
ok = #4bffdc
info = #0a6cf5

user = ${colors.info}
volume = ${colors.info}
backlight = ${colors.info}
date = ${colors.info}
time = ${colors.info}
temp = ${colors.warn}
temp_warn = ${colors.alert}
battery_down = ${colors.warn}
battery_low = ${colors.alert}
battery_up = ${colors.ok}
battery_full = ${colors.ok}
cpu = ${colors.ok}
cpu_warn = ${colors.warn}
mem = ${colors.ok}
mem_warn = ${colors.warn}

[bar/mainbar0]
inherit = bar/barbase
modules-left = workspace0
[module/workspace0]
type = custom/script
exec = leftwm-state -w 0 -t $SCRIPTPATH/template.liquid
tail = true

[bar/mainbar1]
inherit = bar/barbase
; modules-left = workspace1
; [module/workspace1]
; type = custom/script
; exec = leftwm-state -w 1 -t $SCRIPTPATH/template.liquid
; tail = true

[bar/mainbar2]
inherit = bar/barbase
; modules-left = workspace2
; [module/workspace2]
; type = custom/script
; exec = leftwm-state -w 2 -t $SCRIPTPATH/template.liquid
; tail = true

[bar/mainbar3]
inherit = bar/barbase
; modules-left = workspace3
; [module/workspace3]
; type = custom/script
; exec = leftwm-state -w 3 -t $SCRIPTPATH/template.liquid
; tail = true


[bar/barbase]
bottom = true

width = ${env:width}
offset-x = 0
offset-y = 0
; radius = 16.0
monitor = ${env:monitor}

height = 27
fixed-center = true
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 20
padding-right = 20
module-margin-left = 0
module-margin-right = 10
font-0 = misc fixed:pixelsize=10;1
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = wuncon siji:pixelsize=10;1
modules-center = memory cpu time battery temperature
modules-right = xbacklight pulseaudio user

[module/ewmh]
type = internal/xworkspaces
label-active = " %icon% %name%  "
label-active-foreground = ${colors.foreground-alt}
label-active-background = ${colors.background-alt}
label-active-underline = ${colors.primary}
label-occupied = " %icon% %name%  "
label-occupied-underline = ${colors.secondary}
label-urgent = " %icon% %name%  "
label-urgent-foreground = ${colors.foreground}
label-urgent-background = ${colors.background}
label-urgent-underline  = ${colors.alert}
label-empty = " %icon% %name%  "
label-empty-foreground = ${colors.foreground}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

; Separator in between workspaces
; label-separator = |

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

; Separator in between workspaces
; label-separator = |

[module/xbacklight]
type = internal/xbacklight

format = <label>
label = BL %percentage%%
format-underline = ${colors.backlight}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-underline = ${colors.cpu}
format-warn-underline = ${colors.cpu_warn}
label = CPU %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.mem}
warn-percentage = 90
label-warn-underline = ${colors.mem_warn}
label = MEM %percentage_used%%

; bar-width = 50
; bar-indicator = |
; bar-indicator-foreground = #fff
; bar-indicator-font = 2
; bar-fill = ─
; bar-fill-font = 2
; bar-fill-foreground = #fff
; bar-empty = ─
; bar-empty-font = 2
; bar-empty-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5
date = " %Y-%m-%d"
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.date}
label = %date%

[module/time]
type = internal/date
interval = 1
time = "%H:%M:%S"
date = "%Y-%m-%d"
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.time}
label = %time% | %date%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}
format-volume-underline = ${colors.volume}

label-muted = muted
label-muted-foreground = ${colors.foreground-alt}

; bar-volume-width = 10
; bar-volume-foreground-0 = #55aa55
; bar-volume-foreground-1 = #55aa55
; bar-volume-foreground-2 = #55aa55
; bar-volume-foreground-3 = #55aa55
; bar-volume-foreground-4 = #55aa55
; bar-volume-foreground-5 = #f5a70a
; bar-volume-foreground-6 = #ff5555
; bar-volume-gradient = false
; bar-volume-indicator = |
; bar-volume-indicator-font = 2
; bar-volume-fill = ─
; bar-volume-fill-font = 2
; bar-volume-empty = ─
; bar-volume-empty-font = 2
; bar-volume-empty-foreground = ${colors.foreground-alt}

[module/alsa]
type = internal/alsa

format-volume = <label-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}
format-underline = #4bffdc

format-muted-foreground = ${colors.foreground-alt}
label-muted = muted

; bar-volume-width = 10
; bar-volume-foreground-0 = #55aa55
; bar-volume-foreground-1 = #55aa55
; bar-volume-foreground-2 = #55aa55
; bar-volume-foreground-3 = #55aa55
; bar-volume-foreground-4 = #55aa55
; bar-volume-foreground-5 = #f5a70a
; bar-volume-foreground-6 = #ff5555
; bar-volume-gradient = false
; bar-volume-indicator = |
; bar-volume-indicator-font = 2
; bar-volume-fill = ─
; bar-volume-fill-font = 2
; bar-volume-empty = ─
; bar-volume-empty-font = 2
; bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98
low-at = 10

format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.battery_up}

format-discharging = <animation-discharging> <label-discharging>
format-discharging-underline = ${colors.battery_down}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${colors.batery_full}

format-low = LOW <label-low>
format-low-underline = ${colors.battery_low}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <label>
format-underline = ${colors.temp}
format-warn = <label-warn>
format-warn-underline = ${colors.temp_warn}

label = %temperature-c%
label-warn = %temperature-c%

hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon6/temp1_input

[module/user]
type = custom/script
exec = $SCRIPTPATH/userinfo.lua
tail = false
format-underline = ${colors.user}

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

;[global/wm]
;margin-top = 5
;margin-bottom = 5

; vim:ft=dosini
